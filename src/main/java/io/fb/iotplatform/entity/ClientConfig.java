package io.fb.iotplatform.entity;

import java.util.List;

public class ClientConfig {

	private String indexPowerMeter;
	private String indexMeterStatus;
	private String indexKwhlog;
	private String indexGensetActivity;
	private String indexEbstatus;
	private String indexAlertMembers;
	private String indexAlertStatus;

	private List<AlertMember> alertMember;

	public String getIndexAlertMembers() {
		return indexAlertMembers;
	}

	public void setIndexAlertMembers(String indexAlertMembers) {
		this.indexAlertMembers = indexAlertMembers;
	}

	public String getIndexAlertStatus() {
		return indexAlertStatus;
	}

	public void setIndexAlertStatus(String indexAlertStatus) {
		this.indexAlertStatus = indexAlertStatus;
	}

	public String getIndexPowerMeter() {
		return indexPowerMeter;
	}

	public void setIndexPowerMeter(String indexPowerMeter) {
		this.indexPowerMeter = indexPowerMeter;
	}

	public String getIndexMeterStatus() {
		return indexMeterStatus;
	}

	public void setIndexMeterStatus(String indexMeterStatus) {
		this.indexMeterStatus = indexMeterStatus;
	}

	public String getIndexKwhlog() {
		return indexKwhlog;
	}

	public void setIndexKwhlog(String indexKwhlog) {
		this.indexKwhlog = indexKwhlog;
	}

	public String getIndexGensetActivity() {
		return indexGensetActivity;
	}

	public void setIndexGensetActivity(String indexGensetActivity) {
		this.indexGensetActivity = indexGensetActivity;
	}

	public String getIndexEbstatus() {
		return indexEbstatus;
	}

	public void setIndexEbstatus(String indexEbstatus) {
		this.indexEbstatus = indexEbstatus;
	}
	
	


	public List<AlertMember> getAlertMembers() {
		return alertMember;
	}

	public void setAlertMembers(List<AlertMember> alertMember) {
		this.alertMember = alertMember;
	}

	@Override
	public String toString() {
		return "ClientConfig [indexPowerMeter=" + indexPowerMeter + ", indexMeterStatus=" + indexMeterStatus
				+ ", indexKwhlog=" + indexKwhlog + ", indexGensetActivity=" + indexGensetActivity + ", indexEbstatus="
				+ indexEbstatus + ", indexAlertMembers=" + indexAlertMembers + ", indexAlertStatus=" + indexAlertStatus
				+ ", alertMembers=" + alertMember + "]";
	}

}
