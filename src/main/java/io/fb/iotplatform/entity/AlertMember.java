package io.fb.iotplatform.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName="foo")
public class AlertMember {
	
	
	@Id
	private String id;
	private String name;
	private String ph_no;
	private String email;
	private Long createdtime;
	private Long updatedtime;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPh_no() {
		return ph_no;
	}
	public void setPh_no(String ph_no) {
		this.ph_no = ph_no;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Long getCreatedtime() {
		return createdtime;
	}
	public void setCreatedtime(Long createdtime) {
		this.createdtime = createdtime;
	}
	public Long getUpdatedtime() {
		return updatedtime;
	}
	public void setUpdatedtime(Long updatedtime) {
		this.updatedtime = updatedtime;
	}
	
	

}
