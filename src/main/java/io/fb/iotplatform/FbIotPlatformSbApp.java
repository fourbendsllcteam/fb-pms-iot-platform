package io.fb.iotplatform;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;

import com.google.gson.Gson;

@Configuration
//@EnableAutoConfiguration(exclude={ElasticsearchAutoConfiguration.class, RestClientAutoConfiguration.class})
@SpringBootApplication
public class FbIotPlatformSbApp extends SpringBootServletInitializer {

	public static String CLIENT_INFO_STATIC;


	/*
	 * @Value("${client.info}") private String clientInfo;
	 * 
	 * @Value("${client.info}") public void setClientInfoStatic(String clientInfo) {
	 * FbIotPlatformSbApp.CLIENT_INFO_STATIC = clientInfo; }
	 */

	public static final Logger LOG = LoggerFactory.getLogger(FbIotPlatformSbApp.class);
	@Autowired
	Runnable MessageListener;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(FbIotPlatformSbApp.class);
	}

	@Bean
	public CommandLineRunner schedulingRunner(TaskExecutor executor) {
		return new CommandLineRunner() {
			public void run(String... args) throws Exception {
				executor.execute(MessageListener);
			}
		};
	}
	
	@Bean
	public Gson gsonObject() {
		return new Gson();
	}


	
	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
		SpringApplication.run(FbIotPlatformSbApp.class, args);
		//LOG.info("************" + CLIENT_INFO_STATIC + "************");
		LOG.info("FB-IOT PLATFORM STARTED*******");		
	
		
	}

}