package io.fb.iotplatform.service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.fb.iotplatform.entity.AlertMember;
import io.fb.iotplatform.entity.ClientConfig;
import io.fb.iotplatform.utils.AS_C;
import io.fb.iotplatform.utils.EB_C;
import io.fb.iotplatform.utils.GA_C;
import io.fb.iotplatform.utils.KWH_C;
import io.fb.iotplatform.utils.MS_C;
import io.fb.iotplatform.utils.PM_C;

@Service
public class RuleEngineImpl implements RuleEngine {

	public static final Logger LOG = LoggerFactory.getLogger(RuleEngineImpl.class);

	@Value("${client.data}")
	private String clientDataFileName;

	@Value("${elasticsearch.host}")
	private String ES_HOST;

	@Value("${elasticsearch.port}")
	private int ES_PORT;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	RestHighLevelClient highLevelClient;
	@Autowired
	ElasticsearchOperations elasticsearchTemplate;

	@Autowired
	RestHighLevelClient elasticSearchClient;

	private boolean isLastPMDExists, isLastMSExists, isLastKWHExists, isLastGAExists, isLastEBExists = false;

	@Override
	public void processData(JsonObject currentPowerMeter, String topic) {


		byte _case = 0;

		ClientConfig clientConfig = new ClientConfig();

		Integer meterId = currentPowerMeter.get(PM_C.METER_ID).getAsInt(); // Integer.parseInt(currentPMD.get(PM_C.METER_ID).toString());
		Integer gen = currentPowerMeter.get(PM_C.GEN).getAsInt();// Integer.parseInt(currentPMD.get(PM_C.GEN).toString());
		Integer dgId = currentPowerMeter.get(PM_C.DG_ID).getAsInt();// Integer.parseInt(currentPMD.get(PM_C.DG_ID).toString());
		String recordId = currentPowerMeter.get(PM_C.ID).getAsString();// (currentPMD.get(PM_C.ID).toString());
		String kwh = currentPowerMeter.get(PM_C.KWH).getAsString();// currentPMD.get(PM_C.KWH).toString();

		try {
			clientConfig = setClientBasedAttributes(topic);
		} catch (FileNotFoundException e) {
			LOG.info("ISSUE WITH READING FILE..FileNotFoundException.." + e.getMessage());
			LOG.debug(e.getMessage(), e);
		} catch (IOException e) {
			LOG.info("ISSUE WITH READING FILE..IOException.." + e.getMessage());
			LOG.debug(e.getMessage(), e);
		} catch (ParseException e) {
			LOG.info("ISSUE WITH READING FILE..ParseException.." + e.getMessage());
			LOG.debug(e.getMessage(), e);
		}

		if (currentPowerMeter.get(PM_C.TV).getAsFloat() > 0) {
			
			LOG.info("RE: TV > 0, CONTINUING...");
			JsonObject lastPowerMeter = getLastRecordOfMeterData(meterId, clientConfig);

			if (isLastPMDExists) {
				Integer lastGen = lastPowerMeter.get(PM_C.GEN).getAsInt();
				LOG.info("RE: LAST POWER METER DATA FOUND..");
				if (isLastPMDExists && lastGen == gen)
					_case = 1; // same value case
				else if (lastGen != gen && gen == 0)
					_case = 2; // diff value - running on eb case
				else if (lastGen != gen && gen == 1)
					_case = 3; // diff value - running on dg case
			} else {
				_case = 4;
			}

		}

		switch (_case) {
		case 1:
			executeSameValueCaseProcess(currentPowerMeter, clientConfig, meterId, gen, recordId, dgId);
			break;
		case 2:
			executeDiffValueEBCaseProcess(currentPowerMeter, clientConfig, meterId, dgId, kwh, gen, recordId);
			break;
		case 3:
			executeDiffValueDGCaseProcess(currentPowerMeter, clientConfig, meterId, dgId, kwh, gen, recordId);
			break;
		case 4:
			executeNoLasRecordCaseProcess(currentPowerMeter, clientConfig, meterId, gen, recordId, dgId);
			break;
		default:
			LOG.info("RE: TV < 0, TERMINATING***********");
		}

	}

	private void executeSameValueCaseProcess(JsonObject currentPowerMeter, ClientConfig clientConfig, Integer meterId,
			Integer gen, String recordId, Integer dgId) {
		LOG.info("RE: SAME VALUE CASE");
		// saving power meter data
		savePowerMeterData(currentPowerMeter, clientConfig);

		// inserting or updating meter status data
		JsonObject lastMeterStatus = getLastMeterStatusData(meterId, clientConfig);

		if (isLastMSExists) {
			updateMeterStatus(lastMeterStatus, gen, recordId, dgId, clientConfig);
		} else {
			insertMeterStatus(meterId, gen, recordId, dgId, clientConfig);
		}
	}

	private void executeDiffValueEBCaseProcess(JsonObject currentPowerMeter, ClientConfig clientConfig, Integer meterId,
			Integer dgId, String kwh, Integer gen, String recordId) {

		LOG.info("RE: RUNNING ON EB CASE..");
		// update genset activity and eb status table
		executeEbProcess(currentPowerMeter, clientConfig);
		// update kwh log will also be done in this method
		insertKwhLog(meterId, dgId, kwh, gen, clientConfig);
		if (isAlertEnabled(clientConfig)) {
			notificationService.sendSms(clientConfig.getAlertMembers(), "Your DG has been turned OFF");
			notificationService.sendEmail(clientConfig.getAlertMembers(), "Your DG has been turned OFF",
					"<h1>Your DG has been turned OFF</h1>",
					"This email was sent through Amazon SES from FB IoT Platform");
		}

		// saving power meter data
		savePowerMeterData(currentPowerMeter, clientConfig);

		// inserting or updating meter status data
		JsonObject lastMeterStatus = getLastMeterStatusData(meterId, clientConfig);

		if (isLastMSExists) {
			updateMeterStatus(lastMeterStatus, gen, recordId, dgId, clientConfig);
		} else {
			insertMeterStatus(meterId, gen, recordId, dgId, clientConfig);
		}
	}

	private void executeDiffValueDGCaseProcess(JsonObject currentPowerMeter, ClientConfig clientConfig, Integer meterId,
			Integer dgId, String kwh, Integer gen, String recordId) {

		LOG.info("RE: DIFF VALUE CASE");
		LOG.info("RE: RUNNING ON DG CASE..");
		// insert into genset activity and eb status table
		executeDgProcess(currentPowerMeter, clientConfig);
		// update kwh log will also be done in this method
		insertKwhLog(meterId, dgId, kwh, gen, clientConfig);

		if (isAlertEnabled(clientConfig)) {
			notificationService.sendSms(clientConfig.getAlertMembers(), "Your DG has been turned ON");
			notificationService.sendEmail(clientConfig.getAlertMembers(), "Your DG has been turned ON",
					"<h1>Your DG has been turned ON</h1>",
					"This email was sent through Amazon SES from FB IoT Platform");
		}

		// saving power meter data
		savePowerMeterData(currentPowerMeter, clientConfig);

		// inserting or updating meter status data
		JsonObject lastMeterStatus = getLastMeterStatusData(meterId, clientConfig);

		if (isLastMSExists) {
			updateMeterStatus(lastMeterStatus, gen, recordId, dgId, clientConfig);
		} else {
			insertMeterStatus(meterId, gen, recordId, dgId, clientConfig);
		}

	}

	private void executeNoLasRecordCaseProcess(JsonObject currentPowerMeter, ClientConfig clientConfig, Integer meterId,
			Integer gen, String recordId, Integer dgId) {
		LOG.info("RE: NO LAST RECS..FIRST ENTRY...");
		// saving power meter data
		savePowerMeterData(currentPowerMeter, clientConfig);
		// inserting meter status
		insertMeterStatus(meterId, gen, recordId, dgId, clientConfig);
	}

	private void updateMeterStatus(JsonObject meterStatus, Integer gen, String powerMeterRecId, Integer dg_id,
			ClientConfig clientConfig) {
		LOG.info("RE: UPDATING METER STATUS..");

		meterStatus.addProperty(MS_C.UPDATEDTIME, new Timestamp(System.currentTimeMillis()).getTime());
		meterStatus.addProperty(MS_C.STATUS, gen);
		meterStatus.addProperty(MS_C.RECORD_ID, powerMeterRecId);
		meterStatus.addProperty(MS_C.DG_ID, dg_id);

		try {
			if (isIndexExists(clientConfig.getIndexMeterStatus())) {
				indexTheDocument(clientConfig.getIndexMeterStatus(), meterStatus.get(MS_C.ID).getAsString(),
						meterStatus.toString());
				LOG.info("RE: METER STATUS UPDATED...");
			}
		} catch (Exception e) {
			LOG.info("RE: ISSUE WITH UPDATING METER STATUS.." + e.getMessage());
			LOG.debug(e.getMessage(), e);
		}

	}

	private void insertMeterStatus(Integer meter_id, Integer gen, String powerMeterRecId, Integer dgId,
			ClientConfig clientConfig) {

		LOG.info("RE: SAVING METER STATUS..");

		JsonObject meterStatus = new JsonObject();

		meterStatus.addProperty(MS_C.ID, UUID.randomUUID().toString());
		meterStatus.addProperty(MS_C.METER_ID, meter_id);
		meterStatus.addProperty(MS_C.STATUS, gen);
		meterStatus.addProperty(MS_C.RECORD_ID, powerMeterRecId);
		meterStatus.addProperty(MS_C.DG, gen);
		meterStatus.addProperty(MS_C.DG_ID, dgId);
		meterStatus.addProperty(MS_C.CREATEDTIME, new Timestamp(System.currentTimeMillis()).getTime());
		meterStatus.addProperty(MS_C.UPDATEDTIME, new Timestamp(System.currentTimeMillis()).getTime());

		try {
			if (isIndexExists(clientConfig.getIndexMeterStatus())) {
				indexTheDocument(clientConfig.getIndexMeterStatus(), meterStatus.get(MS_C.ID).getAsString(),
						meterStatus.toString());
				LOG.info("RE: METER STATUS INSERTED...");
			}
		} catch (Exception e) {
			LOG.info("RE: ISSUE WITH INSERTING METER STATUS.." + e.getMessage());
			LOG.debug(e.getMessage(), e);
		}

	}

	private void savePowerMeterData(JsonObject powerMeter, ClientConfig clientConfig) {

		powerMeter.addProperty("created", new Timestamp(System.currentTimeMillis()).getTime());
		powerMeter.addProperty("createdtime", new Timestamp(System.currentTimeMillis()).getTime());

		LOG.info("RE: SAVING POWER METER DATA..");

		try {
			if (isIndexExists(clientConfig.getIndexPowerMeter())) {
				indexTheDocument(clientConfig.getIndexPowerMeter(), powerMeter.get(PM_C.ID).getAsString(),
						powerMeter.toString());
				LOG.info("RE: POWER METER DATA SAVED...");
			}
		} catch (Exception e) {
			LOG.info("RE: ISSUE WITH SAVING POWER METER DATA.." + e.getMessage());
			LOG.debug(e.getMessage(), e);
		}

	}

	private void executeEbProcess(JsonObject currentPowerMeter, ClientConfig clientConfig) {

		LOG.info("RE: EXECUTING EB PROCESS..");

		LOG.info("RE:UPDATING GENSET ACTIVITY..");

		Integer meterId = currentPowerMeter.get(PM_C.METER_ID).getAsInt();
		JsonObject lastGensetActivityData = getLastGensetActivity(meterId, clientConfig);

		try {

			if (isLastGAExists) {
				lastGensetActivityData.addProperty(GA_C.LAST_OFF_TIME,
						new Timestamp(System.currentTimeMillis()).getTime());
				if (isIndexExists(clientConfig.getIndexGensetActivity())) {
					indexTheDocument(clientConfig.getIndexGensetActivity(),
							lastGensetActivityData.get(GA_C.ID).getAsString(), lastGensetActivityData.toString());
					LOG.info("RE: GENSET ACTIVITY UPDATED...");
				}
			}

		} catch (Exception e) {
			LOG.info("RE:ISSUE WITH UPDATING GENSET ACTIVITY.." + e.getMessage());
			LOG.debug(e.getMessage(), e);
		}

		JsonObject lastEbStatus = getLastEbStatus(meterId, clientConfig);
		LOG.info("RE: UPDATING EB STATUS..");

		try {

			if (isLastEBExists) {
				lastEbStatus.addProperty(EB_C.STATUS_ON, new Timestamp(System.currentTimeMillis()).getTime());
				if (isIndexExists(clientConfig.getIndexEbstatus())) {
					indexTheDocument(clientConfig.getIndexEbstatus(), lastEbStatus.get(EB_C.ID).getAsString(),
							lastEbStatus.toString());
					LOG.info("RE:EB STATUS UPDATED...");
				}
			}

		} catch (Exception e) {

			LOG.info("RE:ISSUE WITH EB UPDATING STATUS.." + e.getMessage(), e);

		}

	}

	private void executeDgProcess(JsonObject currentPowerMeter, ClientConfig clientConfig) {

		Integer meterId = currentPowerMeter.get(PM_C.METER_ID).getAsInt();
		Integer dgId = currentPowerMeter.get(PM_C.DG_ID).getAsInt();

		LOG.info("RE: EXECUTING DG PROCESS..");

		insertGensetActivity(dgId, meterId, clientConfig);
		insertEbStatus("OFF", (Integer) meterId, clientConfig);

	}

	private void updateKwhLog(Integer meter_id, String kwhEnd, ClientConfig clientConfig) {
		LOG.info("RE: UPDATING KWHLOG ");

		JsonObject lastKwhLogData = getLastRecordOfKwhLog(meter_id, clientConfig);

		try {
			if (isLastKWHExists) {
				if (isIndexExists(clientConfig.getIndexKwhlog())) {

					if (lastKwhLogData.get(KWH_C.ID).getAsString() != null) {
						lastKwhLogData.addProperty(KWH_C.LAST_OFF_TIME,
								new Timestamp(System.currentTimeMillis()).getTime());
						lastKwhLogData.addProperty(KWH_C.KWH_END, kwhEnd);
						indexTheDocument(clientConfig.getIndexKwhlog(), lastKwhLogData.get(KWH_C.ID).getAsString(),
								lastKwhLogData.toString());
						LOG.info("RE: KWH LOG UPDATED...");
					}
				}
			}
		} catch (Exception e) {
			LOG.info("RE: ISSUE WITH UPDATING KWH LOG.." + e.getMessage());
			LOG.debug(e.getMessage(), e);
		}

	}

	private void insertKwhLog(Integer meter_id, Integer dg_id, String kwh, Integer gen, ClientConfig clientConfig) {

		LOG.info("RE: UPDATING KWHLOG PART");

		String kwh_start = null;
		String kwh_end = null;
		Integer status = 0;

		if (gen == 1)
			status = 1;

		@SuppressWarnings("unused")
		JsonObject lastKwhLogData = getLastRecordOfKwhLog(meter_id, clientConfig);

		if (isLastKWHExists) {
			LOG.info("RE: LAST KWH LOG FOUND CASE");
			updateKwhLog(meter_id, kwh, clientConfig);
			LOG.info("RE: LAST KWH LOG UPDATION DONE");
			kwh_start = kwh;
		} else {
			LOG.info("RE: LAST KWH LOG NOT FOUND CASE");
			kwh_start = kwh;
		}

		LOG.info("RE: INSERTING NEW REC IN KWH LOG");

		JsonObject kwhLogData = new JsonObject();

		kwhLogData.addProperty(KWH_C.ID, UUID.randomUUID().toString());
		kwhLogData.addProperty(KWH_C.DEVICE_ID, meter_id);
		kwhLogData.addProperty(KWH_C.LAST_ON_TIME, new Timestamp(System.currentTimeMillis()).getTime());

		kwhLogData.addProperty(KWH_C.DG_STATUS, status);
		kwhLogData.addProperty(KWH_C.KWH_START, kwh_start);
		kwhLogData.addProperty(KWH_C.KWH_END, kwh_end);

		try {
			if (isIndexExists(clientConfig.getIndexKwhlog())) {
				indexTheDocument(clientConfig.getIndexKwhlog(), kwhLogData.get(KWH_C.ID).getAsString(),
						kwhLogData.toString());
				LOG.info("RE: KWH LOG INSERTED...");
			}
		} catch (Exception e) {
			LOG.info("RE: ISSUE WITH INSERTING KWH LOG.." + e.getMessage());
			LOG.debug(e.getMessage(), e);
		}

	}

	private void insertEbStatus(String status, Integer meter_id, ClientConfig clientConfig) {

		LOG.info("RE: SAVING EB STATUS...");

		JsonObject ebStatusData = new JsonObject();
		ebStatusData.addProperty(EB_C.ID, UUID.randomUUID().toString());
		ebStatusData.addProperty(EB_C.STATUS, status);
		ebStatusData.addProperty(EB_C.METER_ID, meter_id);
		if (status.equals("OFF")) {
			ebStatusData.addProperty(EB_C.STATUS_OFF, new Timestamp(System.currentTimeMillis()).getTime());
			// ebStatusData.addProperty(EB_C.STATUS_ON, "");
		} else {
			ebStatusData.addProperty(EB_C.STATUS_ON, new Timestamp(System.currentTimeMillis()).getTime());
		}

		try {
			if (isIndexExists(clientConfig.getIndexEbstatus())) {
				indexTheDocument(clientConfig.getIndexEbstatus(), ebStatusData.get(EB_C.ID).getAsString(),
						ebStatusData.toString());
				LOG.info("RE: EB STATUS INSERTED...");
			}
		} catch (Exception e) {
			LOG.info("RE: ISSUE WITH INSERTING EB STATUS.." + e.getMessage());
			LOG.debug(e.getMessage(), e);
		}

	}

	private void insertGensetActivity(Integer dg_id, Integer meter_id, ClientConfig clientConfig) {

		LOG.info("RE: SAVING GENSET ACTIVITY...");

		JsonObject gensetActivityData = new JsonObject();
		gensetActivityData.addProperty(GA_C.ID, UUID.randomUUID().toString());
		gensetActivityData.addProperty(GA_C.LAST_ON_TIME, new Timestamp(System.currentTimeMillis()).getTime());
		gensetActivityData.addProperty(GA_C.DG_ID, dg_id);
		gensetActivityData.addProperty(GA_C.DEVICE_ID, meter_id);
		// gensetActivityData.addProperty(GA_C.LAST_OFF_TIME, "");

		try {
			if (isIndexExists(clientConfig.getIndexGensetActivity())) {
				indexTheDocument(clientConfig.getIndexGensetActivity(), gensetActivityData.get(GA_C.ID).getAsString(),
						gensetActivityData.toString());
				LOG.info("RE: GENSET ACTIVITY INSERTED...");
			}
		} catch (Exception e) {
			LOG.info("RE: ISSUE WITH INSERTING GENSET ACTIVITY.." + e.getMessage());
			LOG.debug(e.getMessage(), e);
		}

	}

	private JsonObject getLastGensetActivity(Integer meter_id, ClientConfig clientConfig) {

		LOG.info("RE: GETTING LAST REC OF GENSET ACTIVITY ");

		JsonObject gensetActivtiyData = new JsonObject();

		if (isIndexExists(clientConfig.getIndexPowerMeter())) {
			SearchResponse searchResponse = searchLastRecord(clientConfig.getIndexGensetActivity(), GA_C.DEVICE_ID,
					meter_id, GA_C.LAST_ON_TIME, SortOrder.DESC, 0, 1);

			if (searchResponse.getHits().getTotalHits() > 0) {
				SearchHit searchHit = searchResponse.getHits().getAt(0);
				LOG.info("RE: LAST GENSET ACTIVITY REC ID: " + searchHit.getId());
				String jsonResponse = searchHit.getSourceAsString();
				gensetActivtiyData = JsonParser.parseString(jsonResponse).getAsJsonObject();
				LOG.info("RE: LAST GENSET ACTIVITY DG ID VALUE:: " + gensetActivtiyData.get(GA_C.DG_ID).getAsInt());

				isLastGAExists = true;
			} else {
				LOG.info("RE: NO LAST GENSET ACTIVITY FOUND");
				isLastGAExists = false;
			}

		}
		return gensetActivtiyData;

	}

	private JsonObject getLastMeterStatusData(Integer meter_id, ClientConfig clientConfig) {

		LOG.info("RE: GETTING LAST REC OF METER STATUS DATA ");

		JsonObject meterStatusData = new JsonObject();

		if (isIndexExists(clientConfig.getIndexMeterStatus())) {
			SearchResponse searchResponse = searchLastRecord(clientConfig.getIndexMeterStatus(), MS_C.METER_ID,
					meter_id, MS_C.CREATEDTIME, SortOrder.DESC, 0, 1);

			if (searchResponse.getHits().getTotalHits() > 0) {

				SearchHit searchHit = searchResponse.getHits().getAt(0);

				LOG.info("RE: LAST METER STATUS DATA REC ID: " + searchHit.getId());
				String jsonResponse = searchHit.getSourceAsString();

				meterStatusData = JsonParser.parseString(jsonResponse).getAsJsonObject();
				LOG.info("RE: LAST METER STATUS DATA LAST UDPATED TIME: " + meterStatusData.get(MS_C.ID).getAsString());

				isLastMSExists = true;
			} else {
				LOG.info("RE: NO LAST METER STATUS FOUND");
				isLastMSExists = false;
			}

		}
		return meterStatusData;
	}

	private JsonObject getLastEbStatus(Integer meter_id, ClientConfig clientConfig) {

		LOG.info("RE: GETTING LAST REC OF EB STATUS");

		JsonObject lastEBStatusData = new JsonObject();

		if (isIndexExists(clientConfig.getIndexEbstatus())) {
			SearchResponse searchResponse = searchLastRecord(clientConfig.getIndexEbstatus(), EB_C.METER_ID, meter_id,
					EB_C.STATUS_OFF, SortOrder.DESC, 0, 1);

			if (searchResponse.getHits().getTotalHits() > 0) {
				SearchHit searchHit = searchResponse.getHits().getAt(0);
				LOG.info("RE: LAST EB STATUS DATA REC ID: " + searchHit.getId());
				String jsonResponse = searchHit.getSourceAsString();
				lastEBStatusData = JsonParser.parseString(jsonResponse).getAsJsonObject();
				LOG.info("RE: LAST EB STATUS DATA LAST STATUS: " + lastEBStatusData.get(EB_C.STATUS));
				isLastEBExists = true;
			} else {
				LOG.info("RE: NO LAST EB STATUS FOUND");
				isLastEBExists = false;
			}

		}
		return lastEBStatusData;

	}

	private JsonObject getLastRecordOfKwhLog(Integer meter_id, ClientConfig clientConfig) {

		LOG.info("RE: GETTING LAST REC OF KWH LOG ");

		JsonObject kwhLogData = new JsonObject();

		if (isIndexExists(clientConfig.getIndexPowerMeter())) {
			SearchResponse searchResponse = searchLastRecord(clientConfig.getIndexKwhlog(), KWH_C.DEVICE_ID, meter_id,
					"last_on_time", SortOrder.DESC, 0, 1);
			if (searchResponse.getHits().getTotalHits() > 0) {
				SearchHit searchHit = searchResponse.getHits().getAt(0);
				LOG.info("RE: LAST KWH LOG DATA REC ID: " + searchHit.getId());
				String jsonResponse = searchHit.getSourceAsString();
				kwhLogData = JsonParser.parseString(jsonResponse).getAsJsonObject();
				LOG.info("RE: LAST KWH START VALUE: " + kwhLogData.get(KWH_C.KWH_START));

				isLastKWHExists = true;
			} else {
				LOG.info("RE: NO LAST KWH LOG FOUND");
				isLastKWHExists = false;
			}

		}
		return kwhLogData;

	}

	private JsonObject getLastRecordOfMeterData(Integer meter_id, ClientConfig clientConfig) {

		LOG.info("RE: GETTING LAST REC OF POWER METER DATA ");

		JsonObject lastPowerMeterData = new JsonObject();

		if (isIndexExists(clientConfig.getIndexPowerMeter())) {
			SearchResponse searchResponse = searchLastRecord(clientConfig.getIndexPowerMeter(), PM_C.METER_ID, meter_id,
					"createdtime", SortOrder.DESC, 0, 1);
			if (searchResponse.getHits().getTotalHits() > 0) {
				SearchHit searchHit = searchResponse.getHits().getAt(0);
				LOG.info("RE: LAST POWER METER REC ID: " + searchHit.getId());
				String jsonResponse = searchHit.getSourceAsString();
				lastPowerMeterData = JsonParser.parseString(jsonResponse).getAsJsonObject();
				LOG.info("RE: LAST POWER METER GEN VALUE: " + lastPowerMeterData.get(PM_C.GEN).getAsString());
				isLastPMDExists = true;
			} else {
				LOG.info("RE: NO LAST POWER METER FOUND: ");
				isLastPMDExists = false;
			}
		}
		return lastPowerMeterData;

	}

	private Boolean isAlertEnabled(ClientConfig clientConfig) {
		Boolean result = true;

		try {
			SearchResponse searchResponse = new SearchResponse();
			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			searchSourceBuilder.from(0);
			searchSourceBuilder.size(1);
			searchSourceBuilder.query(QueryBuilders.matchAllQuery());

			SearchRequest searchRequest = new SearchRequest(clientConfig.getIndexAlertStatus());
			searchRequest.source(searchSourceBuilder);
			searchResponse = elasticSearchClient.search(searchRequest, RequestOptions.DEFAULT);

			if (searchResponse.getHits().getTotalHits() > 0) {
				SearchHit searchHit = searchResponse.getHits().getAt(0);
				LOG.info("RE: ALERT STATUS ID: " + searchHit.getId());
				String jsonResponse = searchHit.getSourceAsString();
				JsonObject alertStatus = JsonParser.parseString(jsonResponse).getAsJsonObject();

				LOG.info("RE: ALERT STATUS: " + alertStatus.get(AS_C.ALERT_FLAG).getAsString());
				result = alertStatus.get(AS_C.ALERT_FLAG).getAsBoolean();

			}

		} catch (Exception e) {
			LOG.info("RE: ISSUE WITH SEARCHING: " + e.getMessage());
			LOG.debug(e.getMessage(), e);
		}
		return result;

	}

	private ClientConfig setClientBasedAttributes(String topic)
			throws FileNotFoundException, IOException, ParseException {

		LOG.info("JSON FILE: " + clientDataFileName);
		Object obj = new JSONParser().parse(new FileReader(clientDataFileName));
		JSONObject jo = (JSONObject) obj;

		JSONObject clientConfigJSONObj = (JSONObject) jo.get(topic);

		ClientConfig clientConfig = new ClientConfig();
		clientConfig.setIndexPowerMeter((String) clientConfigJSONObj.get("indexPowerMeter"));
		clientConfig.setIndexMeterStatus((String) clientConfigJSONObj.get("indexMeterStatus"));
		clientConfig.setIndexKwhlog((String) clientConfigJSONObj.get("indexKwhlog"));
		clientConfig.setIndexGensetActivity((String) clientConfigJSONObj.get("indexGensetActivity"));
		clientConfig.setIndexEbstatus((String) clientConfigJSONObj.get("indexEbstatus"));
		clientConfig.setIndexAlertMembers((String) clientConfigJSONObj.get("indexAlertMembers"));
		clientConfig.setIndexAlertStatus((String) clientConfigJSONObj.get("indexAlertStatus"));

		List<AlertMember> alertMembers = new ArrayList<AlertMember>();

		try {

			SearchResponse searchResponse = new SearchResponse();

			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			searchSourceBuilder.query(QueryBuilders.matchAllQuery());

			SearchRequest searchRequest = new SearchRequest(clientConfig.getIndexAlertMembers());
			searchRequest.source(searchSourceBuilder);
			searchResponse = elasticSearchClient.search(searchRequest, RequestOptions.DEFAULT);

			if (searchResponse.getHits().getTotalHits() > 0) {
				LOG.info("TOTAL ALERT MEMBERS: " + searchResponse.getHits().getTotalHits());
				SearchHits hits = searchResponse.getHits();
				for (SearchHit hit : hits) {
					LOG.info("ALERT MEMBERS ID: " + hit.getId());
					String alertMembersAsJson = new String(hit.getSourceAsString());
					Gson g = new Gson();
					alertMembers.add(g.fromJson(alertMembersAsJson, AlertMember.class));
				}
			}

		} catch (ElasticsearchStatusException esEx) {
			LOG.info(esEx.getMessage());
			LOG.debug(esEx.getMessage(), esEx);

		} catch (Exception e) {
			LOG.info(e.getMessage());
			LOG.debug(e.getMessage(), e);
		}
		alertMembers.stream().forEach(alertMember -> System.out.println(alertMember.getName()));
		clientConfig.setAlertMembers(alertMembers);
		return clientConfig;
	}

	private boolean isIndexExists(String indexName) {
		boolean result = false;
		try {
			GetIndexRequest request = new GetIndexRequest(indexName);
			request.local(true);
			request.humanReadable(true);
			request.includeDefaults(false);
			result = elasticSearchClient.indices().exists(request, RequestOptions.DEFAULT);
			LOG.info("RE: IS INDEX EXITS: " + result);
		} catch (Exception e) {
			LOG.info("RE: ISSUE WITH CHECKING INDEX EXISTS: " + e.getMessage());
			LOG.debug(e.getMessage(), e);
		}
		return result;
	}

	private IndexResponse indexTheDocument(String indexName, String docId, String jsonString) {
		IndexResponse indexResponse = new IndexResponse();
		try {
			IndexRequest request = new IndexRequest(indexName, "_doc").id(docId).source(jsonString, XContentType.JSON);
			indexResponse = elasticSearchClient.index(request, RequestOptions.DEFAULT);
			LOG.info("RE: DATE INDEXED: " + indexResponse);
		} catch (Exception e) {
			LOG.info("RE: ISSUE WITH INDEXING: " + e.getMessage());
			LOG.debug(e.getMessage(), e);
		}
		return indexResponse;
	}

	private SearchResponse searchLastRecord(String indexName, String fieldName, Integer meter_id, String sortedBy,
			SortOrder sortOrder, int from, int size) {
		SearchResponse searchResponse = new SearchResponse();
		try {

			BoolQueryBuilder query = new BoolQueryBuilder();
			query.must(new MatchQueryBuilder(fieldName, meter_id));

			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			searchSourceBuilder.from(from);
			searchSourceBuilder.size(size);
			searchSourceBuilder.sort(new FieldSortBuilder(sortedBy).order(sortOrder));
			searchSourceBuilder.query(query);

			SearchRequest searchRequest = new SearchRequest(indexName);
			searchRequest.source(searchSourceBuilder);

			searchResponse = elasticSearchClient.search(searchRequest, RequestOptions.DEFAULT);
		} catch (Exception e) {
			LOG.info("RE: ISSUE WITH SEARCHING: " + e.getMessage());
			LOG.debug(e.getMessage(), e);
		}
		return searchResponse;
	}

}
