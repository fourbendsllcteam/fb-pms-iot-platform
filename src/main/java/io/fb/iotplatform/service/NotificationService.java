package io.fb.iotplatform.service;

import java.util.List;

import io.fb.iotplatform.entity.AlertMember;

public interface NotificationService {	
	
	
	

	//Boolean sendSms(List<String> toPhoneNum, String messageContent);

	//Boolean sendEmail(List<String> emailAddressList, String emailSubject, String HTMLBODY, String TEXTBODY);

	Boolean sendSms(List<AlertMember> phoneNumberList, String messageContent);

	Boolean sendEmail(List<AlertMember> alertMembersList, String emailSubject, String HTMLBODY, String TEXTBODY);

}
