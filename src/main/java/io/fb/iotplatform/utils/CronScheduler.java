package io.fb.iotplatform.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class CronScheduler {

	public static final Logger LOG = LoggerFactory.getLogger(CronScheduler.class);

	private static final String CHECK_METER_STATUS_URL = "http://demopms.4blabs.com/scheduler/checkmeterStatus";
	private static final String SEND_ALERT_URL = "http://demopms.4blabs.com/notification/send_alert";

	//@Scheduled(cron = "*/20 * * * * *") // 20 SECS
	@Scheduled(cron = "0 0/10 * * * ?") //10 MINS
	public void checkMeterStatus() throws IOException {
		LOG.info("CJS: HITTING CHECK METER STATUS ROUTER..." + Calendar.getInstance().getTime());

		try {
			URL obj = new URL(CHECK_METER_STATUS_URL);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			int responseCode = con.getResponseCode();
			// LOG.info("GET Response Code :: " + responseCode);
			if (responseCode == HttpURLConnection.HTTP_OK) { // success
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				// print result
				// LOG.info(response.toString());
			}

		} catch (Exception e) {
			LOG.info("CJS: ISSUE WITH CHECKING METER STATUS");
		}
	}

	//@Scheduled(cron = "*/20 * * * * *") // 20 SECS
	@Scheduled(cron = "0 0/5 * * * ?") //5 MINS
	public void sendAlert() {
		LOG.info("CJS: HITTING SEND ALERT ROUTER..." + Calendar.getInstance().getTime());
		try {
			URL obj = new URL(SEND_ALERT_URL);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			int responseCode = con.getResponseCode();
			// LOG.info("GET Response Code :: " + responseCode);
			if (responseCode == HttpURLConnection.HTTP_OK) { // success
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				// print result
				// LOG.info(response.toString());
			}

		} catch (Exception e) {
			LOG.info("CJS: ISSUE WITH SENDING ALERT");
		}
	}
	
}
