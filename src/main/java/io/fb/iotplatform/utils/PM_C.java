package io.fb.iotplatform.utils;

public interface PM_C {

	public static final String ID = "id", CREATED = "created", CREATED_TIME = "createdtime", BC = "bc", BP = "bp",
			BV = "bv", BYV = "byv", DG_ID = "dg_id", GEN_ID = "gen_id", FRE = "fre", GEN = "gen", KVA = "kva",
			KWH = "kwh", METER_ID = "meter_id", PF = "pf", RBV = "rbv", RC = "rc", RP = "rp", RV = "rv", TC = "tc",
			TP = "tp", TPV = "tpv", TV = "tv", YC = "yc", YP = "yp", YRV = "yrv", YV = "yv";

}
