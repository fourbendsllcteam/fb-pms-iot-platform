package io.fb.iotplatform.utils;

public interface GA_C {

	public static final String ID = "id", DEVICE_ID = "device_id", DEVICE_NAME = "device_name", DG_ID = "dg_id",
			FUEL = "fuel", FUEL_CONSUMED = "fuel_consumed", HOURS = "hours", LAST_OFF_TIME = "last_off_time",
			LAST_ON_TIME = "last_on_time", LASTOFFTIME = "lastofftime", LASTONTIME = "lastontime", RUNNING_HOURS = "running_hours";

}
