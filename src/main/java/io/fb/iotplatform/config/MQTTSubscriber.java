package io.fb.iotplatform.config;

import java.sql.Timestamp;
import java.util.UUID;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

//import io.fb.iotplatform.service.RuleEngineImpl;
import io.fb.iotplatform.service.RuleEngine;
import io.fb.iotplatform.utils.PM_C;

@Component
public class MQTTSubscriber extends MQTTConfig implements MqttCallback, MQTTSubscriberBase {



	@Autowired
	private RuleEngine ruleEngine;

	public static final Logger LOG = LoggerFactory.getLogger(MQTTSubscriber.class);

	private String brokerUrl = null;
	final private String colon = ":";
	// final private String clientId = "liveClient1";

	// localClientUdhaya
	// liveClientFBIOT
	private String clientId = "localClientUdhaya";

	private MqttClient mqttClient = null;
	private MqttConnectOptions connectionOptions = null;
	private MemoryPersistence persistence = null;

	public MQTTSubscriber() {
		this.config();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.paho.client.mqttv3.MqttCallback#connectionLost(java.lang.
	 * Throwable)
	 */
	@Override
	public void connectionLost(Throwable cause) {
		LOG.info("Connection Lost");
		LOG.info("Causes:" + cause);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.paho.client.mqttv3.MqttCallback#messageArrived(java.lang.String,
	 * org.eclipse.paho.client.mqttv3.MqttMessage)
	 */
	@Override
	public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
		// Called when a message arrives from the server that matches any
		// subscription made by the client
		String time = new Timestamp(System.currentTimeMillis()).toString();

		LOG.info("***********************************************************************");
		LOG.info("Message Arrived at Time: " + time + "  Topic: " + topic + "  Message: "
				+ new String(mqttMessage.getPayload()));
		LOG.info("***********************************************************************");

		String jsonResponse = new String(mqttMessage.getPayload());

		try {
			
			JsonObject currentPowerMeterData = JsonParser.parseString(jsonResponse).getAsJsonObject();
			
			currentPowerMeterData.addProperty(PM_C.ID, UUID.randomUUID().toString());
			if (currentPowerMeterData.get(PM_C.GEN_ID) == null)
				currentPowerMeterData.addProperty(PM_C.DG_ID, 0);
			else
				currentPowerMeterData.addProperty(PM_C.DG_ID, currentPowerMeterData.get(PM_C.GEN_ID).getAsInt());			
		

			ruleEngine.processData(currentPowerMeterData, topic);

			
		} catch (Exception e) {			
			LOG.info("Data can't be sent to rule engine" + e.getMessage());
			LOG.debug( e.getMessage(), e);

		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.paho.client.mqttv3.MqttCallback#deliveryComplete(org.eclipse.paho
	 * .client.mqttv3.IMqttDeliveryToken)
	 */
	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// Leave it blank for subscriber

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.monirthought.mqtt.subscriber.MQTTSubscriberBase#subscribeMessage(java.
	 * lang.String)
	 */
	@Override
	public void subscribeMessage(String topic) {
		try {
			this.mqttClient.subscribe(topic, this.qos);
			LOG.info(String.format("Subscribed to %s sucessfully", topic));
		} catch (MqttException me) {
			LOG.info("Issue with subscription" + me.getMessage(), me);

		}
	}

	@Override
	public void unsubscribeMessage(String topic) {
		// TODO Auto-generated method stub
		try {
			this.mqttClient.unsubscribe(topic);
			LOG.info(String.format("Unsubscribed to %s sucessfully", topic));
		} catch (MqttException me) {
			LOG.info("Issue with unsubscription" + me.getMessage(), me);

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.monirthought.mqtt.subscriber.MQTTSubscriberBase#disconnect()
	 */
	public void disconnect() {
		try {
			this.mqttClient.disconnect();
		} catch (MqttException me) {
			LOG.error("ERROR", me);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.monirthought.config.MQTTConfig#config()
	 */
	@Override
	protected void config() {

		this.brokerUrl = this.TCP + this.broker + colon + this.port;
		this.persistence = new MemoryPersistence();
		this.connectionOptions = new MqttConnectOptions();
		try {
			this.mqttClient = new MqttClient(brokerUrl, clientId, persistence);
			this.connectionOptions.setCleanSession(false);
			this.connectionOptions.setAutomaticReconnect(true);
			this.mqttClient.connect(this.connectionOptions);
			this.mqttClient.setCallback(this);
		} catch (MqttException me) {
			LOG.error("Connection Error: " + me.getMessage(), me);
		}

	}

}
