package io.fb.iotplatform.config;

import java.io.FileReader;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MessageListener implements Runnable {

	@Autowired
	MQTTSubscriberBase subscriber;

	@Value("${client.data}")
	private String clientDataFileName;

	public static final Logger LOG = LoggerFactory.getLogger(MessageListener.class);

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		try {
			LOG.info("JSON FILE: " + clientDataFileName);
			Object obj = new JSONParser().parse(new FileReader(clientDataFileName));
			JSONObject jo = (JSONObject) obj;
			JSONArray topics = (JSONArray) jo.get("subscription-topics");
			topics.stream().forEach(t -> subscriber.subscribeMessage((String) t));
		} catch (Exception e) {
			// TODO: handle exception
			LOG.error("Can't connect to MQTT, check broker is running", e.getMessage(), e);
		}

	}

}
