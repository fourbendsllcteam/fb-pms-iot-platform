package io.fb.iotplatform.config;

public abstract class MQTTConfig {

	//@Value("${mqtt.broker}")
	protected String broker="demopms.4blabs.com";//demopms.4blabs.com
	
	//@Value("${mqtt.username}")
	protected String userName="fbiot"; //fbiot
	
	//@Value("${mqtt.password}")
	protected String password="welcome123"; //welcome123
	
	//@Value("${mqtt.qos}")
	protected int qos=2; //2
	
	//@Value("${mqtt.port}")
	protected Integer port=1883; /* Default port 1883 */
	
	protected Boolean hasSSL = false; /* By default SSL is disabled */
	protected final String TCP = "tcp://";
	protected final String SSL = "ssl://";
	
	
	





	/**
	 * Custom Configuration
	 * 
	 * @param broker
	 * @param port
	 * @param ssl
	 * @param withUserNamePass
	 */
	//protected abstract void config(String broker, Integer port, Boolean ssl, Boolean withUserNamePass);

	/**
	 * Default Configuration
	 */
	protected abstract void config();
}